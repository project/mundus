<?php

/**
 * @file
 * Add custom theme settings to the sub-theme.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function mundus_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  // Buttons.
  $form['button'] = [
    '#type' => 'details',
    '#title' => t('Button'),
    '#open' => TRUE,
    '#tree' => TRUE,
  ];
  $form['button']['size'] = [
    '#type' => 'select',
    '#title' => t('Button size'),
    '#default_value' => theme_get_setting('button.size'),
    '#options' => [
      'default' => t('Basic'),
      'tiny' => t('Tiny'),
      'small' => t('Small'),
      'large' => t('Large'),
      ],
  ];
  $form['button']['fill'] = [
    '#type' => 'select',
    '#title' => t('Button fill'),
    '#default_value' => theme_get_setting('button.fill'),
    '#options' => [
      'default' => t('Default'),
      'hollow' => t('Hollow'),
      'clear' => t('Clear'),
    ],
  ];

    // Labels.
    $form['label'] = [
        '#type' => 'details',
        '#title' => t('Label'),
        '#open' => TRUE,
        '#tree' => TRUE,
    ];

    $form['label']['color'] = [
        '#type' => 'select',
        '#title' => t('Label coloring'),
        '#default_value' => theme_get_setting('label.color'),
        '#options' => [
            'default' => t('No color'),
            'primary' => t('Primary color'),
            'secondary' => t('Secondary color'),
            'success' => t('Success color'),
            'alert' => t('Alert color'),
            'warning' => t('Warning color'),
        ],
    ];
}
