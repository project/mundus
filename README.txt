INTRODUCTION

Mundus is a theme based on ZURB Foundation framework.

REQUIREMENTS

This theme requires Drupal core >= 9.0.
It also requires ZURB Foundation theme, and related modules ZURB Orbit and XY Grid Layouts.
If you want to be able to change theme colors then color module is required too.

INSTALLATION

Install as you would normally install a contributed Drupal theme.

This theme uses npm which you will need to install as a prerequisite for
developing your theme, although once in production it is not necessary for
general use.
Once you have ensured npm is installed, run this command at the root of
your sub theme:
  - `npm install`
Finally, run `gulp` to run the Sass compiler, or 'gulp watch' which
will re-run every time you save a Sass file. Press Ctrl-C to break out of
watching files.
