<?php

/**
 * @file
 * Add your custom theme override functions here.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'text' => t('Text Color'),
    'background' => t('Background Color'),
    'success' => t('Success color'),
    'warning' => t('Warning color'),
    'alert' => t('Alert color'),
    'black' => t('Black'),
    'white' => t('White'),
    'lightgray' => t('Light gray'),
    'mediumgray' => t('Medium gray'),
    'darkgray' => t('Dark gray'),
  ],

  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Default'),
      'colors' => [
        'primary' => '#f6bd60',
        'secondary' => '#f28482',
        'text' => '#0a0a0a',
        'background' => '#faf9f9',
        'success' => '#95d5b2',
        'warning' => '#ffbf69',
        'alert' => '#cc4b37',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#f7ede2',
        'mediumgray' => '#f5cac3',
        'darkgray' => '#84a59d',
      ],
    ],
    'light' => [
      'title' => t('Light'),
      'colors' => [
        'primary' => '#809BCE',
        'secondary' => '#EAC4D5',
        'text' => '#0a0a0a',
        'background' => '#ffffff',
        'success' => '#D7E9B9',
        'warning' => '#FFFBAC',
        'alert' => '#FD8A8A',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#D6EADF',
        'mediumgray' => '#B8E0D2',
        'darkgray' => '#95B8D1',
      ],
    ],
    'sunset' => [
      'title' => t('Sunset'),
      'colors' => [
        'primary' => '#390099',
        'secondary' => '#9e0059',
        'text' => '#0a0a0a',
        'background' => '#fcf7f6',
        'success' => '#0a9396',
        'warning' => '#fff275',
        'alert' => '#d62828',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#ffbd00',
        'mediumgray' => '#ff5400',
        'darkgray' => '#ff0054',
      ],
    ],
    'nature' => [
      'title' => t('Nature'),
      'colors' => [
        'primary' => '#bc4b51',
        'secondary' => '#5b8e7d',
        'text' => '#0a0a0a',
        'background' => '#FAF8ED',
        'success' => '#A7D397',
        'warning' => '#E7B10A',
        'alert' => '#862B0D',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#f4e285',
        'mediumgray' => '#f4e285',
        'darkgray' => '#8cb369',
      ],
    ],
    'happy' => [
      'title' => t('Happy'),
      'colors' => [
        'primary' => '#0802A3',
        'secondary' => '#FF4B91',
        'text' => '#0a0a0a',
        'background' => '#F9F2ED',
        'success' => '#8ac926',
        'warning' => '#F6F54D',
        'alert' => '#ea3546',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#FFCF96',
        'mediumgray' => '#FFCD4B',
        'darkgray' => '#FF7676',
      ],
    ],
    'foundation' => [
      'title' => t('Foundation'),
      'colors' => [
        'primary' => '#1779ba',
        'secondary' => '#767676',
        'text' => '#0a0a0a',
        'background' => '#fefefe',
        'success' => '#3adb76',
        'warning' => '#ffae00',
        'alert' => '#cc4b37',
        'black' => '#0a0a0a',
        'white' => '#fefefe',
        'lightgray' => '#e6e6e6',
        'mediumgray' => '#cacaca',
        'darkgray' => '#8a8a8a',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'css/color.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],

  // Preview files.
  'preview_library' => 'mundus/color.preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'mundus'),
      ],
    ],
  ],
];
